package com.gitlab.zenyuk_java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class A001_TwoSumTest {

  A001_TwoSum classUnderTest= new A001_TwoSum();

  @Test
  void twoSum() {
    // act
    int[] result = classUnderTest.twoSum(new int[] {1, 2, 3, 4, 5, 9}, 7);

    // assert
    Assertions.assertEquals(1, result[0]);
    Assertions.assertEquals(4, result[1]);
  }
}
