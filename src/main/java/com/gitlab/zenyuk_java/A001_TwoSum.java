package com.gitlab.zenyuk_java;

import java.util.Arrays;

public class A001_TwoSum {
  public static void main(String[] args) {
    var clazz = new A001_TwoSum();
    int[] result = clazz.twoSum(new int[] {1, 2, 3, 4, 5, 9}, 7);
    System.out.println(Arrays.toString(result));
  }

  public int[] twoSum(int[] nums, int target) {
    for (int leftIndex = 0; leftIndex + 1 < nums.length; leftIndex++) {
      for (int rightIndex = 1; rightIndex < nums.length; rightIndex++) {
        if (leftIndex == rightIndex) {
          continue;
        }
        int sum = nums[leftIndex] + nums[rightIndex];
        if (sum == target) {
          return new int[] {leftIndex, rightIndex};
        }
      }
    }
    throw new IllegalArgumentException("No two sum solution");
  }
}
